package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class Calculator {
    /**
     * Map of operators with their priorities used in {@link #convertToPostfix(String)}.
     */
    private static final Map<String, Integer> operatorMap = new HashMap<>();

    static {
        operatorMap.put("+", 0);
        operatorMap.put("-", 0);
        operatorMap.put("*", 1);
        operatorMap.put("/", 1);
    }

    private boolean hasHigherOrEqualPrecedence(String op1, String op2) {
        return (operatorMap.containsKey(op1) &&
                operatorMap.get(op1) >= operatorMap.get(op2));
    }

    /**
     * Evaluates statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1+38)*4.5-1/2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            Queue<String> postfixQueue = convertToPostfix(statement);
            Double result = evaluatePostfix(postfixQueue);

            DecimalFormat decimalFormat = new DecimalFormat("#.####");
            return decimalFormat.format(result);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts statement from infix notation to reverse Polish notation using shunting-yard algorithm.
     * If statement is invalid, postfix expression is also invalid.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1+38)*4.5-1/2.</code>
     * @return queue of tokens (numbers and operators) in postfix notation<br>
     * Example: <code>[1, 38, +, 4.5, *, 1, 2, /, -]</code>
     */
    private Queue<String> convertToPostfix(String statement) {
        LinkedList<String> operatorStack = new LinkedList<>();
        Queue<String> output = new LinkedList<>();

        for (String token : statement.split("(?<=[-+*/()])|(?=[-+*/()])")) {
            if (operatorMap.containsKey(token)) {
                while (!operatorStack.isEmpty() &&
                        hasHigherOrEqualPrecedence(operatorStack.peek(), token))
                    output.offer(operatorStack.pop());
                operatorStack.push(token);
            } else if (token.equals("("))
                operatorStack.push(token);

            else if (token.equals(")")) {
                if (operatorStack.peek() != null)
                    while (!operatorStack.peek().equals("("))
                        output.offer(operatorStack.pop());
                operatorStack.pop();
            } else output.offer(token);
        }

        while (!operatorStack.isEmpty())
            output.offer(operatorStack.pop());
        return output;
    }

    /**
     * Evaluates postfix expression processed from left to right using a stack.
     * If expression is invalid, unchecked exception is being thrown.
     *
     * @param postfixQueue queue of tokens (numbers and operators) in postfix notation<br>
     *                     Example: <code>[1, 38, +, 4.5, *, 1, 2, /, -]</code>
     * @return expression value
     */
    private Double evaluatePostfix(Queue<String> postfixQueue) {
        LinkedList<Double> stack = new LinkedList<>();

        postfixQueue.forEach(token -> {
            switch (token) {
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-":
                    stack.push(-stack.pop() + stack.pop());
                    break;
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    double divisor = stack.pop();
                    if (divisor == 0) throw new ArithmeticException("/ by zero");
                    stack.push(stack.pop() / divisor);
                    break;
                default:
                    stack.push(Double.parseDouble(token));
            }
        });

        return stack.pop();
    }
}
