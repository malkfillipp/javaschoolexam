package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     * Greedy tries to compare the element from x with an element from y till the matching
     * or the end. If there is a match, the new trial start with a next element from x.
     * If there is not, it is not possible.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null)
            throw new IllegalArgumentException("x is null");
        if (y == null)
            throw new IllegalArgumentException("y is null");

        if (x.isEmpty()) return true;

        ListIterator xit = x.listIterator();
        ListIterator yit = y.listIterator();

        while (yit.hasNext()) {
            if (!xit.hasNext()) return true;
            else if (!xit.next().equals(yit.next()))
                xit.previous();
        }

        return false;
    }
}
