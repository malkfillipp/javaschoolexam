package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     * Size of inputNumbers should be triangular number.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            if (inputNumbers == null || inputNumbers.contains(null))
                throw new IllegalArgumentException();

            int sideSize = getSideSize(inputNumbers.size());
            Collections.sort(inputNumbers);

            return getPyramid(inputNumbers, sideSize);
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     * Given arguments should be correct.
     * It is possible to build a pyramid only if size of inputNumbers is triangular number.
     *
     * @param inputNumbers to be used in the pyramid
     * @param sideSize     size of the side of the pyramid
     * @return 2d array with the pyramid inside
     */
    private int[][] getPyramid(List<Integer> inputNumbers, int sideSize) {
        int rowNum = sideSize;
        int colNum = 2 * rowNum - 1;
        int[][] result = new int[rowNum][colNum];

        Iterator<Integer> it = inputNumbers.iterator();

        int half = (colNum - 1) / 2;
        boolean setNumber = true;

        for (int i = 0; i < rowNum; i++, half--) {
            for (int j = half; j < colNum - half; j++) {
                if (setNumber)
                    result[i][j] = it.next();
                setNumber = !setNumber;
            }
            setNumber = !setNumber;
        }

        return result;
    }

    /**
     * Triangular number (T) is equal to the sum of the n natural numbers from 1 to n.
     * Gets n from given triangular number.
     * <p>
     * T = n(n+1)/2 => n = (sqrt(8T + 1) - 1)/2
     * n is triangular number if 8T + 1 is perfect square.
     *
     * @param triangularNumber from which is needed to find n
     * @return n
     * @throws IllegalArgumentException if given number is not triangular
     */
    private int getSideSize(int triangularNumber) throws IllegalArgumentException {
        if (triangularNumber < 0)
            throw new IllegalArgumentException("Given number is not triangular");

        double triangularRoot = Math.sqrt(8 * triangularNumber + 1);

        if (triangularRoot % 1 != 0)
            throw new IllegalArgumentException("Given number is not triangular");

        return ((int) triangularRoot - 1) / 2;
    }
}
